from scapy.all import *
from pathlib import Path

class DHCPD(DHCP_am):
    def setopts(self):
        self.pool = ['192.168.0.120']*1000
        self.netmask = '255.255.255.0'
        self.gw = '192.168.0.1'
        self.broadcast = '192.168.0.255'
        self.network = '192.168.0.0'
        self.lease_time = 30
        self.renewal_time = 20
        self.file = 'outfile.txt'
        self.filename = Path.joinpath(Path.home(), self.file)
        self.lfilter = lambda x: UDP in x and x[UDP].sport == 68
        self.iface = None
        self.optsniff = {'iface': self.iface, 'lfilter': self.lfilter, 'prn': self.get_name_from_packet, 'count': 1}
        self.send_options = {'verbose': 0}
        self.hostname = None

    def get_name_from_packet(self, pkt):
        if self.is_request(pkt):
            _ = self.get_name_from_options(pkt[DHCP].options)
            if self.hostname != _.split('-')[1]:
                self.hostname = _.split('-')[1]
                self.write_hostname()
                print(self.hostname)
            return self.hostname

    def get_name_from_options(self, opts):
        for o in opts:
            if o[0] == 'hostname':
                return o[1]

    def write_hostname(self):
        with open(self.filename, 'a+') as f:
            f.write(self.hostname)
            f.write('\r\n')
