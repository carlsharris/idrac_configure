from openpyxl import load_workbook
from pprint import pprint
from tkinter.filedialog import askopenfilename
from pathlib import Path
import paramiko
from time import sleep
from scapy.all import *
from subprocess import check_call, CalledProcessError,  SubprocessError
from winsound import Beep
from paramiko.ssh_exception import NoValidConnectionsError

success_sound = [ 800, 500 ]

data_cols = ['hostname', 'idracip', 'gateway', 'netmask', 'passwd']

hosts = []
conf.verb = 0

HOMEDIR = str(Path.home())

# Disable warnings to prevent paramiko SSH spam
warnings.filterwarnings('ignore', category=DeprecationWarning)

def show_interface_select():
    print(ifaces)
    print('')
    iface_idx = input('Enter index number of interface to use: ')
    selected = ifaces.dev_from_index(iface_idx)
    return selected

def set_interface_address(iface, address='192.168.0.1', netmask='255.255.255.0'):
    netsh_cmd = f'netsh interface ipv4 set address name={iface.win_index} static {address} {netmask}'
    try:
        check_call(netsh_cmd, shell=True)
    except SubprocessError as e:
        raise(e)
    else:
        print(f'Configured interface {iface.description} with static {address}/{netmask}')
    return

def set_interface_dhcp(iface):
    netsh_cmd = f'netsh interface ipv4 set address name={iface.win_index} source=dhcp'
    try:
        check_call(netsh_cmd, shell=True)
    except SubprocessError as e:
        raise(e)
    else:
        print(f'Configured interface {iface.description} with DHCP')
    return

def get_filename():
    fn = askopenfilename(initialdir=HOMEDIR,
                         filetypes=(("Excel Files", "*.xlsx"),("All Files","*.*")),
                         title="Select a prepared iLoader spreadsheet..."
    )
    return fn

def get_worksheet(fn):
    wb = load_workbook(fn, data_only=True)
    if len(wb.sheetnames) > 1:
        for num, ws in enumerate(wb.sheetnames):
            print(f'{num}: {ws}')
        wsin = input('Enter worksheet id:')
        ws = wb[wb.sheetnames[int(wsin)]]
    else:
        ws = wb[wb.sheetnames[0]]
    return ws

def show_columns(ws):
    for num, col in enumerate(ws.columns):
        if col[0].value is not None:
            print(f'{num}: {col[0].value}')

def get_columns(ws):
    headers = [cell.value for cell in ws['A1':'E1'][0] if cell.value]
    _ = {}
    if data_cols == headers:
        for cell in ws['A1':'E1'][0]:
            _[cell.value] = cell.column - 1
        return _
    for col in data_cols:
        show_columns(ws)
        v = input(f'Select {col} column id:')
        _[col] = int(v)
    return _

def idrac_config(cmd, host='192.168.0.120', username='root', password='calvin'):
    result = False
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        ssh.connect(host, username=username, password=password)
        stdin, stdout, stderr = ssh.exec_command(cmd)
        for line in stdout.readlines():
            if 'Object value modified successfully\n' in line:
                result = True
                break
            else:
                continue
    except (paramiko.AuthenticationException, paramiko.SSHException) as e:
        print(f'Failed to configure host: {e}')
        uin = input('Push Enter to continue or CTRL-C to exit\n')
        result = False
    except (TimeoutError, NoValidConnectionsError) as e:
        print(e)
        print(f'An error occured connection to {host} using {username}:{password}')
        print(f'{cmd}')
        sleep(2)
        idrac_config(cmd, host=host, username=username, password=password)
    finally:
        ssh.close()
        return result

def check_host_online(address='192.168.0.120/32'):
    online = None
    while not online:
        r = arping(address)
        try:
            if r[0].res[0] is not None:
                online = r[0][0][1].src
        except:
            pass
    return online

def wait_for_host(address='192.168.0.120/32'):
    host = check_host_online(address=address)
    if host in hosts:
        sleep(1)
        wait_for_host(address)
    hosts.append(host)
    return host

def run(set_power=True, filename=None):
    if filename is None:
        filename = get_filename()
    ws = get_worksheet(filename)
    colinfo = get_columns(ws)
    srows = input('Row number to start on: [2]')
    if srows == '' or srows is None:
        srows = 2
    for row in ws.iter_rows(min_row=int(srows)):
        hostname = row[colinfo['hostname']].value
        idracip = row[colinfo['idracip']].value
        netmask = row[colinfo['netmask']].value
        gateway = row[colinfo['gateway']].value
        if row[colinfo['passwd']].value is not None:
            passwd = row[colinfo['passwd']].value
        else:
            passwd = 'calvin'
        try:
            if row[colinfo['oldaddr']].value is not None:
                oldaddr = row[colinfo['oldaddr']].value
        except KeyError:
            oldaddr = '192.168.0.120'
        arpaddr = oldaddr + '/32'
        if hostname is not None:
            print(f'Connect to host {hostname}.\r\n')
            host = wait_for_host(arpaddr)
            if set_power is True:
                cmd = f'racadm set System.Power.Hotspare.Enable 0'
                idrac_config(cmd, host=oldaddr, password=passwd)
            cmd = f'racadm setniccfg -s {idracip} {netmask} {gateway}'
            idrac_config(cmd, host=oldaddr, password=passwd)
            print(f'Configured {hostname}({host}) with {idracip}/{netmask}/{gateway}....\r\n')
            Beep(success_sound[0], success_sound[1])
            sleep(2)

if __name__ == '__main__':
    run()
