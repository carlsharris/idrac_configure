def show_interface_select():
    print(ifaces)
    print('')
    iface_idx = input('Enter index number of interface to use: ')
    selected = ifaces.dev_from_index(iface_idx)
    return selected

def set_interface_address(iface, address='192.168.0.1', netmask='255.255.255.0'):
    netsh_cmd = f'netsh interface ipv4 set address name={iface.win_index} static {address} {netmask}'
    try:
        check_call(netsh_cmd, shell=True)
    except SubprocessError as e:
        raise(e)
    else:
        print(f'Configured interface {iface.description} with static {address}/{netmask}')
    return

def set_interface_dhcp(iface):
    netsh_cmd = f'netsh interface ipv4 set address name={iface.win_index} source=dhcp'
    try:
        check_call(netsh_cmd, shell=True)
    except SubprocessError as e:
        raise(e)
    else:
        print(f'Configured interface {iface.description} with DHCP')
    return

def get_filename():
    fn = askopenfilename(initialdir=HOMEDIR,
                         filetypes=(("Excel Files", "*.xlsx"),("All Files","*.*")),
                         title="Select a prepared iLoader spreadsheet..."
    )
    return fn

def get_worksheet(fn):
    wb = load_workbook(fn, data_only=True)
    if len(wb.sheetnames) > 1:
        for num, ws in enumerate(wb.sheetnames):
            print(f'{num}: {ws}')
        wsin = input('Enter worksheet id:')
        ws = wb[wb.sheetnames[int(wsin)]]
    else:
        ws = wb[wb.sheetnames[0]]
    return ws

def show_columns(ws):
    for num, col in enumerate(ws.columns):
        if col[0].value is not None:
            print(f'{num}: {col[0].value}')

def get_columns(ws):
    headers = [cell.value for cell in ws['A1':'E1'][0] if cell.value]
    _ = {}
    if data_cols == headers:
        for cell in ws['A1':'E1'][0]:
            _[cell.value] = cell.column - 1
        return _
    for col in data_cols:
        show_columns(ws)
        v = input(f'Select {col} column id:')
        _[col] = int(v)
    return _

