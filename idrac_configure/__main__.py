import sys
from idrac_configure.dhcpd import DHCPD
import idrac_configure.idrac_configure as idrac
from scapy.all import *
from threading import Thread
from time import sleep
import warnings
import argparse

def main(args=None):
    """The main routine."""

    # Do argument parsing here (eg. with argparse) and anything else
    # you want your project to do.
    parser = argparse.ArgumentParser(description='''
    iDRAC Configuration Utility - idrac_configure
    All arguments are optional and should only be used if you know what you are doing!'
    ''')
    parser.add_argument('-i', '--interface', default=None,
                        help='Index of interface to use, you will be prompted if not provided.')
    parser.add_argument('-a', '--address', nargs=2, default=('192.168.0.1', '255.255.255.0'), metavar=(
        'address', 'netmask'), help='Address and netmask to set local adapter to, only set to reconfigure hosts')
    parser.add_argument('-f', '--filename', default=None,
                        help='Excel file to use for idrac configuration.')
    parser.add_argument('-p', '--set-power', type=bool, default=True,
                        help='Default configures power supply balancing, set to False or 0 to disable (ie: "-p0", "--set-power False")')
    args = parser.parse_args()
    
    address = args.address[0]
    netmask = args.address[1]
    filename = args.filename
    set_power = args.set_power

    d = DHCPD()
    d2 = DHCPD()

    d.setopts()
    d2.setopts()
    t = Thread(target=d2, daemon=True)
    t3 = Thread(target=idrac.run, kwargs={
        'filename': filename, 'set_power': set_power}, daemon=True)
    
    # TODO: Autodetect interface name!
    iface = idrac.show_interface_select()
    idrac.set_interface_address(iface, address, netmask)
    conf.iface = iface.name
    conf.verb = 0

    t.start()
    print('CTRL+C to stop the DHCP service.')
    print('################### DHCP Service Started ###################\n')
    t3.start()
    while True:
        try:
            if t3.is_alive():
                sleep(.5)
            else:
                sys.exit()
        except KeyboardInterrupt:
            print('CTRL-C detected, exitting...')
            idrac.set_interface_dhcp
            sys.exit()
        except SystemExit:
            idrac.set_interface_dhcp
            sys.exit()

if __name__ == "__main__":
    main()
