from setuptools import setup

setup(
    name='idrac_configure',
    version='2020.0203',
    packages=['idrac_configure'],
    entry_points={
        'console_scripts': ['doidrac = idrac_configure.__main__:main'],
    },
    install_requires=[
        'Click',
        "cffi",
        "cryptography",
        "openpyxl",
        "paramiko",
        "scapy",
    ],
    url='https://bitbucket.eng.t-mobile.com/users/charris141/repos/idrac_configure/',
    license='GPL',
    author='Carl S. Harris',
    author_email='carl.harris141@t-mobile.com',
    description='Dell iDRAC configuration utility',
)
