from idrac_configure.dhcpd import DHCPD
from idrac_configure import idrac_configure as idrac
from scapy.all import *
from threading import Thread
from time import sleep
import argparse

parser = argparse.ArgumentParser(description='''
        DHCP Server for iDRAC and iLO configuration
''')
parser.add_argument('-i', '--interface', default=None, help='Index of interface to use, you will be prompted if not provided.')
args = parser.parse_args()

interface_id = args.interface

# TODO: Autodetect interface name!
if interface_id is None:
    iface = idrac.show_interface_select()
    idrac.set_interface_address(iface)
else:
    iface = ifaces.dev_from_index(interface_id)
conf.iface = iface.name


d = DHCPD()
d2 = DHCPD()

d.setopts()
d2.setopts()

d.iface = iface.name
d2.iface = iface.name
thread1 = Thread(target=d2, daemon=True)

def get_serials():
    while thread1.is_alive():
        try:
            hostname = d.sniff()
        except:
            pass
        else:
            print(hostname)

thread2 = Thread(target=get_serials, daemon=True)

def run(thread1, thread2):
    thread1.start()
    thread2.start()
    print('CTRL+C to stop the DHCP service.')
    print('################### DHCP Service Started ###################\n')
    while True:
        try:
            sleep(.5)
        except KeyboardInterrupt:
            print('CTRL-C detected, exitting...')
            exit(0)

if __name__ == '__main__':
    run(thread1, thread2)
