
#### iDRAC Configure Tool
This tool is designed for Data Center Operations to allow rapid configuration of Dell iDRAC in mass.  Tooling to reconfigure iDRAC IPs included, see help(-h).  See [DCO wiki](https://tmobileusa.sharepoint.com/teams/DCO/Polaris/Polaris%20Handbook/iDRAC_Configure%20Installation.aspx) for installation details.

```
usage: run.py [-h] [-i INTERFACE] [-a address netmask] [-f FILENAME] [-p SET_POWER]

iDRAC Configuration Utility - idrac_configure All arguments are optional and should only be used if you know what you
are doing or need to reconfigure hosts!'

optional arguments:
  -h, --help            show this help message and exit
  -i INTERFACE, --interface INTERFACE
                        Index of interface to use, you will be prompted if not provided.
  -a address netmask, --address address netmask
                        Address and netmask to set local adapter to, only set to reconfigure hosts
  -f FILENAME, --filename FILENAME
                        Excel file to use for idrac configuration.
  -p SET_POWER, --set-power SET_POWER,
                        Default configures power supply balancing, set to False or 0 to disable (ie: "-p0", "--set-
                        power False")
```